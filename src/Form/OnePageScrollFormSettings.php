<?php

namespace Drupal\onepage_scroll\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Class OnePageScrollFormSettings.
 */
class OnePageScrollFormSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'onepage_scroll.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'onepagescroll_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('onepage_scroll.settings');

    $form['informations'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('For more informations about plugin options see: https://github.com/peachananr/onepage-scroll'),
    ];

    $form['containerMain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Main container'),
      '#size' => 30,
      '#maxlength' => 64,
      '#default_value' => $config->get('containerMain'),
      '#description' => $this->t('A selector that must be added one level below the body tag in order to make it work full page. Default: .main'),
    ];

    $form['sectionContainer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Section container'),
      '#size' => 30,
      '#maxlength' => 64,
      '#default_value' => $config->get('sectionContainer'),
      '#description' => $this->t('Accepts any kind of selector in case you don\'t want to use section. Default: section'),
    ];

    $form['easing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Easing'),
      '#size' => 30,
      '#maxlength' => 64,
      '#default_value' => $config->get('easing'),
      '#description' => $this->t('Accepts the CSS3 easing animation such "ease", "linear", "ease-in", "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"'),
    ];

    $form['animationTime'] = [
      '#type' => 'number',
      '#title' => $this->t('Animation time'),
      '#size' => 30,
      '#default_value' => $config->get('animationTime'),
      '#description' => $this->t('Let you define how long each section takes to animate. Default: 1000'),
    ];

    $form['pagination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pagination'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('pagination'),
      '#description' => $this->t('You can either show or hide the pagination. Uncheck for hide.'),
    ];

    $form['updateURL'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update URL'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('updateURL'),
      '#description' => $this->t('Check if you want the URL to be updated automatically when the user scroll to each page.'),
    ];

    $form['loop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Loop'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('loop'),
      '#description' => $this->t('You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.'),
    ];

    $form['keyboard'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keyboard'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('keyboard'),
      '#description' => $this->t('You can activate the keyboard controls.'),
    ];

    $form['responsiveFallback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Responsive fallback'),
      '#size' => 30,
      '#default_value' => $config->get('responsiveFallback'),
      '#description' => $this->t('You can fallback to normal page scroll by defining the width of the browser in which you want the responsive fallback to be triggered. For example, set this to 600 and whenever the browser\'s width is less than 600, the fallback will kick in.'),
    ];

    $form['direction'] = [
      '#type' => 'radios',
      '#title' => $this->t('Direction'),
      '#options' => ['vertical' => $this->t('Vertical'), 'horizontal' => $this->t('Horizontal')],
      '#default_value' => $config->get('direction'),
      '#description' => $this->t('Define the direction of the One Page Scroll animation.'),
    ];

    // Load all content types available.
    $node_types = NodeType::loadMultiple();

    // Create options with all content types.
    $node_type_options = [];
    foreach ($node_types as $node_type) {
      $node_type_options[$node_type->id()] = $node_type->label();
    }

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#options' => $node_type_options,
      '#description' => $this->t('Select which content types the plugin should be added to.'),
      '#default_value' => $config->get('content_types') ?: [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('onepage_scroll.settings')
      ->set('containerMain', $form_state->getValue('containerMain'))
      ->set('sectionContainer', $form_state->getValue('sectionContainer'))
      ->set('easing', $form_state->getValue('easing'))
      ->set('animationTime', $form_state->getValue('animationTime'))
      ->set('pagination', $form_state->getValue('pagination'))
      ->set('updateURL', $form_state->getValue('updateURL'))
      ->set('loop', $form_state->getValue('loop'))
      ->set('keyboard', $form_state->getValue('keyboard'))
      ->set('responsiveFallback', $form_state->getValue('responsiveFallback'))
      ->set('direction', $form_state->getValue('direction'))
      ->set('content_types', $form_state->getValue('content_types'))
      ->save();

    // Clear cache to update drupalSettings on script.js.
    drupal_flush_all_caches();
  }

}
