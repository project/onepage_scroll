(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.onepage_scroll = {
    attach: function (context, settings) {
      var $containerMain = drupalSettings.onepage_scroll.containerMain;
      $($containerMain, context)
        .once("onepage_scroll")
        .each(function () {
          var $sectionContainer = drupalSettings.onepage_scroll.sectionContainer;
          var $easing = drupalSettings.onepage_scroll.easing;
          var $animationTime = drupalSettings.onepage_scroll.animationTime;
          var $pagination = drupalSettings.onepage_scroll.pagination;
          var $updateURL = drupalSettings.onepage_scroll.updateURL;
          var $loop = drupalSettings.onepage_scroll.loop;
          var $keyboard = drupalSettings.onepage_scroll.keyboard;
          var $responsiveFallback = drupalSettings.onepage_scroll.responsiveFallback;
          var $direction = drupalSettings.onepage_scroll.direction;

          $($containerMain).onepage_scroll({
            sectionContainer: $sectionContainer,
            easing: $easing,
            animationTime: $animationTime,
            pagination: $pagination,
            updateURL: $updateURL,
            loop: $loop,
            keyboard: $keyboard,
            responsiveFallback: $responsiveFallback,
            direction: $direction,
          });
        });
    },
  };
})(jQuery, Drupal, drupalSettings);
